<table class="form-table">
    <tbody>
    <tr>
        <th scope="row"><label for="ob_artist_parent">Artist Parent Post</label></th>
        <td>
            <select name="ob_artist_parent">
                <option value="">-- Select an artist --</option>
                <?php foreach ( $artists->posts as $artist ): ?>
                    <option value="<?php echo $artist->ID ?>"<?php if ( $artist->ID == $ob_artist_parent ): ?> selected<?php endif; ?>><?php echo $artist->post_title; ?></option>
                <?php endforeach; ?>
            </select>
        </td>
    </tr>
    </tbody>
</table>

<script>
  (function ($) {
    $('select[name=ob_artist_parent]').change( function( event ) {
      $('select[name=ob_album_api_path]').attr('disabled','disabled');
    });
  })(jQuery)
</script>
<?php wp_nonce_field( 'artist_nonce', 'artist_name' ); ?>