<table class="form-table">
	<tbody>
	<tr>
		<th scope="row"><label for="ob_album_alias">Album Alias</label></th>
		<td><input type="text" name="ob_album_alias" value="<?php echo $ob_album_alias; ?>" class="regular-text" /></td>
	</tr>
	<tr>
		<th scope="row"><label for="ob_release_alias">Release Alias</label></th>
		<td><input type="text" name="ob_release_alias" value="<?php echo $ob_release_alias; ?>" class="regular-text" /></td>
	</tr>
    <tr>
        <th scope="row"><label for="ob_album_api_path">API Path</label></th>
        <td>
            <?php if ( !empty( $albums) ): ?>
                <select name="ob_album_api_path" class="regular-text">
                    <option value=""></option>
                    <?php foreach ( $albums as $album ): ?>
                        <option value="<?php echo $album['api_path']; ?>"<?php if ( $album['api_path'] == $ob_album_api_path ): ?> selected<?php endif; ?>><?php echo $album['title']; ?></option>
                    <?php endforeach; ?>
                </select>

                <p>
                    <strong>CAUTION:</strong> Save the post and refresh the screen if the Parent Artist has been changed.
                </p>
            <?php else: ?>
                <input type="text" name="ob_album_api_path" value="<?php echo $ob_album_api_path; ?>" class="regular-text" />
            <?php endif; ?>
        </td>
    </tr>
	</tbody>
</table>


<?php wp_nonce_field( 'meta_nonce', 'meta_name' ); ?>