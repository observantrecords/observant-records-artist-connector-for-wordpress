<table class="form-table">
	<tbody>
    <tr>
        <th scope="row"><label for="ob_artist_api_path">API Path</label></th>
        <td>
            <?php if ( !empty( $artists) ): ?>
                <select name="ob_artist_api_path" class="regular-text">
                    <option value=""></option>
                    <?php foreach ( $artists as $artist ): ?>
                        <option value="<?php echo $artist['api_path']; ?>"<?php if ( $artist['api_path'] == $ob_artist_api_path ): ?> selected<?php endif; ?>><?php echo $artist['display_name']; ?></option>
                    <?php endforeach; ?>
                </select>
            <?php else: ?>
                <input type="text" name="ob_artist_api_path" value="<?php echo $ob_artist_api_path; ?>" class="regular-text" />
            <?php endif; ?>
        </td>
    </tr>
	</tbody>
</table>


<?php wp_nonce_field( 'meta_nonce', 'meta_name' ); ?>