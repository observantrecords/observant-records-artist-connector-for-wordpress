<table class="form-table">
    <tbody>
    <tr>
        <th scope="row"><label for="ob_album_alias">Release Parent Post</label></th>
        <td>
            <select name="ob_release_parent">
                <option value="">-- Select an album --</option>
                <?php foreach ( $albums->posts as $album ): ?>
                    <option value="<?php echo $album->ID ?>"<?php if ( $album->ID == $ob_release_parent ): ?> selected<?php endif; ?>><?php echo $album->post_title; ?></option>
                <?php endforeach; ?>
            </select>
        </td>
    </tr>
    </tbody>
</table>

<?php wp_nonce_field( 'album_nonce', 'album_name' ); ?>