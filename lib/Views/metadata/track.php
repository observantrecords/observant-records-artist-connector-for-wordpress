<table class="form-table">
	<tbody>
	<tr>
		<th scope="row"><label for="ob_release_alias">Release Alias</label></th>
		<td><input type="text" name="ob_release_alias" value="<?php echo $ob_release_alias; ?>" class="regular-text" /></td>
	</tr>
	<tr>
		<th scope="row"><label for="ob_track_alias">Track Alias</label></th>
		<td><input type="text" name="ob_track_alias" value="<?php echo $ob_track_alias; ?>" class="regular-text" /></td>
	</tr>
    <tr>
        <th scope="row"><label for="ob_release_alias">Release API Path</label></th>
        <td>
            <?php if ( !empty( $albums) ): ?>
                <select name="ob_album_api_path" class="regular-text">
                    <option value=""></option>
                    <?php foreach ( $albums as $album ): ?>
                        <option value="<?php echo $album['api_path']; ?>"<?php if ( $album['api_path'] == $ob_album_api_path ): ?> selected<?php endif; ?>><?php echo $album['title']; ?></option>
                    <?php endforeach; ?>
                </select>
            <?php else: ?>
                <input type="text" name="ob_album_api_path" value="<?php echo $ob_album_api_path; ?>" class="regular-text" />
            <?php endif; ?>
        </td>
    </tr>
    <tr>
        <th scope="row"><label for="ob_release_alias">Track API Path</label></th>
        <td>
            <?php if ( !empty( $tracks) ): ?>
                <select name="ob_track_api_path" class="regular-text">
                    <option value=""></option>
                    <?php foreach ( $tracks as $track ): ?>
                        <option value="<?php echo $track['api_path']; ?>"<?php if ( $track['api_path'] == $ob_track_api_path ): ?> selected<?php endif; ?>><?php echo $track['song_title']; ?></option>
                    <?php endforeach; ?>
                </select>
            <?php else: ?>
                <input type="text" name="ob_track_api_path" value="<?php echo $ob_track_api_path; ?>" class="regular-text" />
            <?php endif; ?>
        </td>
    </tr>
	</tbody>
</table>

<?php wp_nonce_field( 'meta_nonce', 'meta_name' ); ?>