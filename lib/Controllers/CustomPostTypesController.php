<?php


namespace ObservantRecords\WordPress\Plugins\ArtistConnector\Controllers;


use ObservantRecords\WordPress\Plugins\ArtistConnector\Controllers\Api\V2\AlbumController;
use ObservantRecords\WordPress\Plugins\ArtistConnector\Controllers\Api\V2\ArtistController;
use ObservantRecords\WordPress\Plugins\ArtistConnector\Controllers\Api\V2\RestApiController as ApiV2;

class CustomPostTypesController
{

    public function __construct() {

    }

    public static function init() {
        add_action( 'init', array( __CLASS__, 'createPostTypes' ) );
        add_action( 'pre_get_posts', array( __CLASS__, 'addQueryVars' ));
        add_filter( 'the_posts', array( __CLASS__, 'populateMeta'), 10, 2 );
    }

    /**
     * createPostTypes
     *
     * createPostTypes() registers all custom post types for the plugin.
     */
    public static function createPostTypes() {

        register_post_type( 'artist', array(
            'labels' => array(
                'name' => 'Artists',
                'singular_name' => 'Artist',
            ),
            'public' => true,
            'menu_position' => 5,
            'hierarchical' => false,
            'supports' => array(
                'title',
                'editor',
                'revisions',
                'thumbnail',
                'excerpt',
                'author',
                'page-attributes',
            ),
            'has_archive' => 'artists',
            'rewrite' => array(
                'slug' => 'artist',
            ),
            'show_in_rest' => true,
        ) );
        register_post_type( 'album', array(
            'labels' => array(
                'name' => 'Albums',
                'singular_name' => 'Album',
            ),
            'public' => true,
            'menu_position' => 5,
            'supports' => array(
                'title',
                'editor',
                'revisions',
                'thumbnail',
                'excerpt',
                'author',
            ),
            'has_archive' => 'releases',
            'rewrite' => array(
                'slug' => 'release',
            ),
            'show_in_rest' => true,
        ) );
        register_post_type( 'track', array(
            'labels' => array(
                'name' => 'Tracks',
                'singular_name' => 'Track',
            ),
            'public' => true,
            'menu_position' => 5,
            'supports' => array(
                'title',
                'editor',
                'revisions',
                'thumbnail',
                'excerpt',
                'author',
            ),
            'has_archive' => 'tracks',
            'rewrite' => array(
                'slug' => 'track'
            ),
            'show_in_rest' => true,
        ) );

        register_taxonomy( 'artists', array(
            'post',
            'artist',
            'album',
            'track'
        ), array(
            'show_in_rest' => true,
            'labels' => array(
                'name' => 'Artists',
                'singular_name' => 'Artist',
            ),
        ) );
        register_taxonomy( 'albums', array(
            'post',
            'album',
            'track'
        ), array(
            'show_in_rest' => true,
            'labels' => array(
                'name' => 'Albums',
                'singular_name' => 'Album',
            ),
        ) );

    }

    public static function addQueryVars( $query ) {
        $artist_endpoint = ApiV2::getApiArtistEndpoint();

        $artist_meta = null;
        if ( !empty( $artist_endpoint ) ) {

            $transient_name = 'obrc_artist_' . md5( $artist_endpoint );

            $result = ApiV2::query( $artist_endpoint, $transient_name );

            $artist_meta = ( !empty( $result['data'] ) ) ? $result['data'] : null;
        }

        if ( !is_admin() && $query->is_main_query() ) {
            if ( $query->is_post_type_archive ) {
                $query->query_vars['obrc_api_path'] = ApiV2::getApiBase();
                $query->query_vars['obrc_api_path_artist'] = ApiV2::getApiArtistEndpoint();
                $query->query_vars['obrc_artist_meta'] = $artist_meta;

                $query->query_vars['posts_per_page'] = -1;
            }

            if ( $query->is_single ) {
                $query->query_vars['obrc_api_path'] = ApiV2::getApiBase();
                $query->query_vars['obrc_api_path_artist'] = ApiV2::getApiArtistEndpoint();
                $query->query_vars['obrc_artist_meta'] = $artist_meta;
            }
        }
    }

    public static function populateMeta( $posts, $query ) {
        if ( !empty( $posts ) ) {
            if ( !is_admin() && $query->query_vars['post_type'] == 'album' ) {

                if ( $query->is_post_type_archive ) {
                    $albums = (!empty($wp_query->query_vars['obrc_api_path_artist'])) ?
                        AlbumController::getAlbumsByArtist( array(
                            'visible' => 1,
                            'orderBy' => 'album_release_date',
                            'order' => 'desc',
                            'showReleases' => true,
                        ) ) :
                        AlbumController::getAllAlbums( array(
                            'visible' => 1,
                            'orderBy' => 'album_release_date',
                            'order' => 'desc',
                            'showReleases' => true,
                        ) );
                }
            }

            $obrc_types = array( 'album', 'artist' );

            if ( !is_admin() && in_array( $query->query_vars['post_type'], $obrc_types ) ) {

                if ( $query->is_post_type_archive ) {
                    $artists = ArtistController::getAllArtists( array() );
                }
            }

            foreach ( $posts as $post ) {

                // Populate artist.
                if ( !is_admin() && $query->query_vars['post_type'] == 'artist' ) {

                    if ($query->is_single) {
                        $post_api_path = get_post_meta( $post->ID, '_ob_artist_api_path', true );
                        if ( !empty( $post_api_path ) ) {
                            $artist = ArtistController::getArtistByPath( $post_api_path );

                            $connector = ( strpos($post_api_path, '/') === 0 ) ? null : '/';
                            $albums_api_path = $query->query_vars['obrc_api_path'] . $connector . $post_api_path . '/albums';
                            $albums = AlbumController::getAlbumsByArtistPath( $albums_api_path );
                            $artist['albums'] = $albums;

                            if ( !empty( $artist ) ) {
                                $post->obrc_meta = $artist;
                            }
                        }
                    }

                    if ( $query->is_post_type_archive ) {
                        $post_api_path = get_post_meta( $post->ID, '_ob_artist_api_path', true );
                        if ( !empty( $artists ) && !empty( $post_api_path ) ) {
                            $artist_filtered = array_filter( $artists, function ( $artist ) use ( $post_api_path ) {
                                return ( $artist['api_path'] == $post_api_path );
                            } );
                            if ( !empty( $artist_filtered ) ) {
                                $post->obrc_meta = array_shift( $artist_filtered );
                            }
                        }
                    }
                }

                if ( !is_admin() && $query->query_vars['post_type'] == 'album' ) {

                    // Populate albums.
                    if ( $query->is_post_type_archive ) {
                        $post_api_path = get_post_meta( $post->ID, '_ob_album_api_path', true );
                        if ( !empty( $post_api_path ) ) {
                            $_album_filtered = array_filter( $albums, function ( $album ) use ( $post_api_path ) {
                                return ( $album['api_path'] == $post_api_path );
                            } );
                            if ( !empty( $_album_filtered ) ) {
                                $album_filtered = array_shift( $_album_filtered );
                                $_artist_filtered = array_filter( $artists, function ( $artist ) use ( $album_filtered ) {
                                    return ( $artist['alias'] == sanitize_title( $album_filtered['artist']) );
                                } );
                                if ( !empty( $_artist_filtered ) ) {
                                    $artist_filtered = array_shift( $_artist_filtered );
                                    $album_filtered['artist_meta'] = $artist_filtered;
                                }
                                $post->obrc_meta = $album_filtered;
                            }
                        }
                    }
                }

                if ( !is_admin() && 'album' == $query->query_vars['post_type'] ) {

                    // Populate album.
                    if ( $query->is_single ) {
                        $post_api_path = get_post_meta( $post->ID, '_ob_album_api_path', true );
                        if ( !empty( $post_api_path ) ) {
                            $album = AlbumController::getAlbumByPath( $post_api_path );
                            if ( !empty( $album ) ) {
                                $post->obrc_meta = $album;
                            }
                        }
                    }
                }
            }
        }
        return $posts;
    }
}