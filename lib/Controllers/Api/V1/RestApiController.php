<?php


namespace ObservantRecords\WordPress\Plugins\ArtistConnector\Controllers\Api\V1;


use ObservantRecords\WordPress\Plugins\ArtistConnector\Models\Albums\Album;
use ObservantRecords\WordPress\Plugins\ArtistConnector\Models\Albums\Release;
use ObservantRecords\WordPress\Plugins\ArtistConnector\Models\Artists\Artist;

class RestApiController
{

    public function __construct() {

    }

    public static function init() {
        add_action( 'rest_api_init', array( __CLASS__, 'setupCustomFields' )  );
        add_action( 'rest_api_init', array( __CLASS__, 'setupCustomEndPoints' )  );
    }

    public static function setupCustomFields() {
        register_rest_field( 'album', 'release_date', array(
                'get_callback' => array( 'ObservantRecords\WordPress\Plugins\ArtistConnector\Controllers\Api\V1\ReleaseController', 'getReleaseDate' ),
                'schema'          => null,
            )
        );

        register_rest_field( 'album', 'catalog_number', array(
                'get_callback' => array( 'ObservantRecords\WordPress\Plugins\ArtistConnector\Controllers\Api\V1\ReleaseController', 'getCatalogNumber' ),
                'schema'          => null,
            )
        );


        register_rest_field( 'album', 'artist', array(
                'get_callback' => array( 'ObservantRecords\WordPress\Plugins\ArtistConnector\Controllers\Api\V1\ReleaseController', 'getReleaseArtist' ),
                'schema'          => null,
            )
        );
    }

    public static function setupCustomEndPoints() {

        register_rest_route( 'obrc/v1', '/artists', array(
            'methods' => 'GET',
            'callback' => array( 'ObservantRecords\WordPress\Plugins\ArtistConnector\Controllers\Api\V1\ArtistController', 'getAllArtists' ),
            'args' => array(
            ),
        ) );

        register_rest_route( 'obrc/v1', '/artist/(?P<artist>[\w]+)/albums', array(
            'methods' => 'GET',
            'callback' => array( 'ObservantRecords\WordPress\Plugins\ArtistConnector\Controllers\Api\V1\AlbumController', 'getAlbumsByArtist' ),
            'args' => array(
                'artist'
            ),
        ) );

        register_rest_route( 'obrc/v1', '/albums', array(
            'methods' => 'GET',
            'callback' => array( 'ObservantRecords\WordPress\Plugins\ArtistConnector\Controllers\Api\V1\AlbumController', 'getAllAlbums' ),
            'args' => array(
                'artist',
            ),
        ) );

        register_rest_route( 'obrc/v1', '/release/(?P<alias>[\w]+)', array(
            'methods' => 'GET',
            'callback' => array( 'ObservantRecords\WordPress\Plugins\ArtistConnector\Controllers\Api\V1\ReleaseController', 'getRelease' ),
            'args' => array(
                'alias'
            ),
        ) );
    }
}