<?php

namespace ObservantRecords\WordPress\Plugins\ArtistConnector\Controllers\Api\V1;

use ObservantRecords\WordPress\Plugins\ArtistConnector\Models\Artists\Artist;

class ArtistController
{

    public static function getAllArtists( $object ) {
        return Artist::all();
    }

    public static function getArtistByAlias( $artist_alias ) {
        $artist = null;

        if ( !empty( $artist_alias ) ) {
            $artist = Artist::where( 'artist_alias', $artist_alias)->first();
        }

        return $artist;
    }

}