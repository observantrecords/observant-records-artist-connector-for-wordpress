<?php

namespace ObservantRecords\WordPress\Plugins\ArtistConnector\Controllers\Api\V1;

use ObservantRecords\WordPress\Plugins\ArtistConnector\Models\Albums\Album;

class AlbumController
{
    public static function getAllAlbums( $object ) {
        $albums = null;

        if ( !empty( $object['artist'] ) ) {
            $artist = $object['artist'];
            $artist = str_replace( '_', '-', $artist );
            $albums = AlbumController::getAlbumsByArtistAlias( $artist );
        } else {
            $albums = Album::with('artist', 'primary_release')->where( 'album_is_visible', true )->orderBy('album_release_date', 'desc')->get();
        }

        return $albums;
    }

    public static function getAlbumsByArtist( $object ) {
        $alias = $object['artist'];
        $alias = str_replace( '_', '-', $alias );
        return self::getAlbumsByArtistAlias( $alias );
    }

    public static function getAlbumsByArtistAlias( $artist_alias ) {
        $albums = null;

        if ( !empty( $artist_alias ) ) {
            $artist = ArtistController::getArtistByAlias( $artist_alias );

            if ( !empty( $artist ) ) {
                $albums = Album::with( ['artist', 'primary_release'] )
                    ->where( 'album_artist_id', $artist->artist_id )
                    ->where( 'album_is_visible', true)
                    ->orderBy('album_release_date', 'desc')
                    ->get();
            }

        } else {
            $albums = Album::with('artist', 'primary_release' )->where( 'album_is_visible', true)->orderBy('album_release_date', 'desc')->get();
        }

        return $albums;
    }



}