<?php


namespace ObservantRecords\WordPress\Plugins\ArtistConnector\Controllers\Api\V1;


use ObservantRecords\WordPress\Plugins\ArtistConnector\Models\Albums\Album;
use const ObservantRecords\WordPress\Plugins\ArtistConnector\PLUGIN_BASE_PATH;

class BlockController
{

    public function __construct() {

    }

    public static function init() {

        add_action( 'init', array( __CLASS__, 'registerReleaseListBlock' ) );

    }

    public static function getReleaseList( $block_attributes, $content ) {

        $output = null;

        if ( $block_attributes['postsToShow'] === null || $block_attributes['postsToShow'] === '' ) {
            $block_attributes['postsToShow'] = 4;
        }

        if ( $block_attributes['artist'] === 'all' || empty( $block_attributes['artist'] ) ) {

            $albums = Album::with('artist', 'primary_release')
                ->where( 'album_is_visible', true )
                ->orderBy('album_release_date', 'desc')
                ->get();

        } else {

            $albums = Album::with( [ 'primary_release', 'artist' ] )
                ->where( 'album_is_visible', true )
                ->orderBy('album_release_date', 'desc')
                ->get()
                ->filter( function ( $a ) use ( $block_attributes ) {
                    $artist_alias = str_replace( '_', '-', $block_attributes['artist'] );
                    return $a->artist->artist_alias == $artist_alias;
                } );

        }

        if ( $albums->count() > 0 ) {

            $albums->each( function ( $album ) {
                $post = new \WP_Query( array(
                    'post_type' => 'album',
                    'meta_query' => array(
                        array(
                            'key' => '_ob_release_alias',
                            'value' => $album->primary_release->release_alias,
                        ),
                    ),
                ) );

                if ( !empty( $post->post ) ) {
                    $album->post = $post->post;
                }
            } );

            $album_entries_all = $albums->filter( function ( $album ) {
                return !empty( $album->post );
            } );

            $album_entries = ( $block_attributes['postsToShow'] > 0 ) ?
                $album_entries_all->slice(0, $block_attributes[ 'postsToShow' ] )
                : $album_entries_all;

            $cards = null;
            $cards .= $album_entries->map( function ( $album ) {
                $permalink = get_permalink( $album->post->ID );
                $artist = $album->artist->artist_display_name;
                $cover_url_base = self::getCdnUri() . '/artists/' . $album->artist->artist_alias . '/albums/' . $album->album_alias . '/' . strtolower($album->primary_release->release_catalog_num) . '/images';
                $cover_image = $cover_url_base . '/cover_front_medium.jpg';
                $card = <<< CARD
    <div class="card">
        <a href="$permalink">
            <img src="$cover_image" class="card-img-top" alt="{$album->album_title}" title="{$album->album_title}" />
        </a>
        <div class="card-body">
            <ul class="list-unstyled">
                <li><a href="$permalink">{$album->post->post_title}</a></li>
                <li>$artist</li>
            </ul>
        </div>
    </div>
CARD;
                return $card;
            } )
            ->implode( "\n" );
        }

        $output .= <<< OUTPUT
<div class="card-deck wp-block-observant-records-release-list">
    $cards
</div>
OUTPUT;


        return $output;
    }

    public static function registerReleaseListBlock() {

        $asset_file = include( dirname( PLUGIN_BASE_PATH ) . '/build/index.asset.php');

        wp_register_script(
            'observant-records-release-list-block',
            plugins_url( 'build/index.js', PLUGIN_BASE_PATH ),
            $asset_file['dependencies'],
            $asset_file['version']
        );

        register_block_type( 'observant-records/release-list', array(
            'editor_script' => 'observant-records-release-list-block',
            'render_callback' => array( __CLASS__, 'getReleaseList' ),
        ) );

    }

    public static function getCdnUri() {
        return OBSERVANTRECORDS_CDN_BASE_URI;
    }

}