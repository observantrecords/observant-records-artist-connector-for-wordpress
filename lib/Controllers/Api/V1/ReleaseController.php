<?php

namespace ObservantRecords\WordPress\Plugins\ArtistConnector\Controllers\Api\V1;

use ObservantRecords\WordPress\Plugins\ArtistConnector\Models\Albums\Release;

class ReleaseController
{
    public static function getRelease( $object ) {
        $alias = $object['alias'];
        $alias = str_replace( '_', '-', $alias );
        return self::getReleaseByAlias( $alias );
    }

    public static function getReleaseDate( $object ) {
        $release_date = null;
        $release = self::getReleaseByPostId( $object );

        if ( !empty ($release ) ) {
            $release_date = date('Y-m-d', strtotime( $release->release_release_date ) );
        }

        return $release_date;
    }

    public static function getCatalogNumber( $object ) {
        $catalog_number = null;
        $release = self::getReleaseByPostId( $object );

        if ( !empty( $release ) ) {
            $catalog_number = $release->release_catalog_num;
        }

        return $catalog_number;
    }

    public static function getReleaseArtist( $object ) {
        $artist = null;
        $release = self::getReleaseByPostId( $object );

        if ( !empty( $release ) ) {
            $artist = $release->album->artist->artist_display_name;
        }

        return $artist;
    }

    public static function getReleaseByPostId( $object ) {
        $release = null;
        $release_alias = get_post_meta( $object['id'], '_ob_release_alias', true );

        if ( !empty( $release_alias ) ) {
            $release = self::getReleaseByAlias( $release_alias );
        }

        return $release;
    }

    public static function getReleaseByAlias( $release_alias ) {
        $release = null;

        if ( !empty( $release_alias ) ) {
            $release = Release::with('album.artist')->where('release_alias', $release_alias)->first();
        }

        return $release;
    }

}