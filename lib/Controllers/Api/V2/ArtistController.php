<?php

namespace ObservantRecords\WordPress\Plugins\ArtistConnector\Controllers\Api\V2;

use ObservantRecords\WordPress\Plugins\ArtistConnector\Models\Artists\Artist;

class ArtistController
{

    public static function getAllArtists( $object ) {
        $endpoint = RestApiController::getApiBase() . '/artists';

        $result = RestApiController::query($endpoint, 'obrc_all_artists');

        if ( !empty( $result['data'] ) ) {
            return $result['data'];
        }

        return $result;
    }

    public static function getArtist( $object ) {
        $endpoint = ( !empty( $object['artist']) )
            ? sprintf( '%s/artist/%s', RestApiController::getApiBase(), $object['artist'])
            : RestApiController::getApiArtistEndpoint();

        $transient = 'obrc_artist_' . md5( $endpoint );

        $result = RestApiController::query( $endpoint, $transient );

        if ( !empty( $result['data'] ) ) {
            return $result['data'];
        }

        return $result;
    }

    public static function getArtistByPath( $path ) {
        $endpoint = RestApiController::getApiBase() . $path;

        $transient = 'obrc_artist' . md5( $endpoint );

        $result = RestApiController::query( $endpoint, $transient );

        if ( !empty( $result['data'] ) ) {
            return $result['data'];
        }

        return $result;
    }

}