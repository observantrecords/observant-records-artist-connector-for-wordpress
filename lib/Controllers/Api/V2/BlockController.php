<?php


namespace ObservantRecords\WordPress\Plugins\ArtistConnector\Controllers\Api\V2;


use const ObservantRecords\WordPress\Plugins\ArtistConnector\PLUGIN_BASE_PATH;

class BlockController
{

    public function __construct() {

    }

    public static function init() {

        add_action( 'init', array( __CLASS__, 'registerReleaseListBlock' ) );

    }

    public static function getReleaseList( $block_attributes, $content ) {

        $output = null;
        $cards = null;

        // Show 4 cards by default.
        if ( $block_attributes['postsToShow'] === null || $block_attributes['postsToShow'] === '' ) {
            $block_attributes['postsToShow'] = 4;
        }

        // Get all albums or all albums by a particular artist.
        if ( $block_attributes['artist'] === 'all' || empty( $block_attributes['artist'] ) ) {

            $albums = AlbumController::getAllAlbums( array(
                'visible' => 1,
                'orderBy' => 'album_release_date',
                'order' => 'desc',
                'showReleases' => true,
            ) );

        } else {

            $albums = AlbumController::getAlbumsByArtist( array(
                'artist' => $block_attributes['artist'],
                'visible' => 1,
                'orderBy' => 'album_release_date',
                'order' => 'desc',
                'showReleases' => true,
            ) );

        }

        if ( count( $albums ) > 0 ) {

            // Get the API paths of the available albums.
            $album_api_paths = array_map( function ( $album ) {
                return $album['api_path'];
            }, $albums );

            // Find posts with the API path as post meta.
            $album_posts = new \WP_Query( array(
                'post_type' => 'album',
                'posts_per_page' => -1,
                'meta_query' => array(
                    array(
                        'key' => '_ob_album_api_path',
                        'value' => $album_api_paths,
                    ),
                ),
            ) );

            // Map the post with the album.
            if ( !empty( $album_posts->posts ) ) {
                foreach ( $albums as $key => $album ) {
                    $api_path_filter = array_filter( $album_posts->posts, function ( $post ) use ( $album ) {
                        $post_api_path = get_post_meta( $post->ID, '_ob_album_api_path', true );
                        return ( empty( $post_api_path ) ) ? false : ( $post_api_path == $album['api_path'] );
                    });
                    if ( count( $api_path_filter ) === 1 ) {
                        $albums[$key]['post'] = array_pop( $api_path_filter );
                    }
                }
            }

            // Filter only those albums with posts.
            $album_entries_all = array_filter( $albums, function ( $album ) {
                return !empty( $album['post'] );
            });

            // Limit the number of albums by the selected number to show.
            $album_entries = array();
            if ( !empty( $album_entries_all ) ) {
                $album_entries = array_slice( $album_entries_all, 0, $block_attributes['postsToShow'] );
            }

            // Build the cards.
            $cards_array = array_map( function ( $album ) use ( $block_attributes ) {

                // Get the artist meta data.
                if ( $block_attributes['artist'] === 'all' || empty( $block_attributes['artist'] ) ) {
                    $parent_artist = get_post_meta( $album['post']->ID, '_ob_artist_parent', true );

                    if ( !empty( $parent_artist ) ) {
                        $parent_api_endpoint = get_post_meta( $parent_artist, '_ob_artist_api_path', true );

                        if ( !empty( $parent_api_endpoint ) ) {
                            $artist_id = preg_replace('/\/artist\//', '', $parent_api_endpoint );

                            if ( !empty( $artist_id ) ) {
                                $artist = ArtistController::getArtist( array(
                                    'artist' => $artist_id,
                                ) );
                            }
                        }
                    }
                } else {
                    $artist = ArtistController::getArtist( array(
                        'artist' => $block_attributes['artist'],
                    ) );
                }

                // Build the permalink and image.
                $permalink = get_permalink( $album['post']->ID );
                $cover_url_base = sprintf(
                    '%s/artists/%s/albums/%s/%s/images',
                    self::getCdnUri(),
                    $artist['alias'],
                    $album['alias'],
                    strtolower( $album['primary_release']['catalog_num'] )
                );
                $cover_image = $cover_url_base . '/cover_front.jpg';
                $cover_image_large = $cover_url_base . '/cover_front_large.jpg';
                $cover_image_medium = $cover_url_base . '/cover_front_medium.jpg';
                $cover_image_small = $cover_url_base . '/cover_front_small.jpg';
                $srcset = sprintf('%s 1425w, %s 713w, %s 356w, %s 178w', $cover_image, $cover_image_large, $cover_image_medium, $cover_image_small);

                // Create the card.
                $card = <<< CARD
    <div class="card">
        <a href="$permalink">
            <img src="$cover_image_large" srcset="$srcset" class="card-img-top" alt="{$album['title']}" title="{$album['title']}" />
        </a>
        <div class="card-body">
            <ul class="list-unstyled">
                <li><a href="$permalink">{$album['title']}</a></li>
                <li>{$artist['display_name']}</li>
            </ul>
        </div>
    </div>
CARD;
                return $card;
            }, $album_entries );

            // Merge the card array into a string.
            $cards = ( !empty( $cards_array ) ) ? implode( "\n", $cards_array ) : '<p>No albums found.</p>';
        } else {
            $cards = '<p>No albums found.</p>';
        }

        // Display the cards.
        $output .= <<< OUTPUT
<div class="card-deck wp-block-observant-records-release-list">
    $cards
</div>
OUTPUT;

        return $output;
    }

    public static function registerReleaseListBlock() {

        $asset_file = include( dirname( PLUGIN_BASE_PATH ) . '/build/index.asset.php');

        wp_register_script(
            'observant-records-release-list-block',
            plugins_url( 'build/index.js', PLUGIN_BASE_PATH ),
            $asset_file['dependencies'],
            $asset_file['version']
        );

        register_block_type( 'observant-records/release-list', array(
            'editor_script' => 'observant-records-release-list-block',
            'render_callback' => array( __CLASS__, 'getReleaseList' ),
            'attributes' => array(
                'postsToShow' => array(
                    'type' => 'number',
                    'default' => 4,
                ),
                'artist' => array(
                    'type' => 'string',
                ),
                'artistDefault' => array(
                    'type' => 'object',
                ),
            ),
        ) );

    }

    public static function getCdnUri() {
        return OBSERVANTRECORDS_CDN_BASE_URI;
    }

}