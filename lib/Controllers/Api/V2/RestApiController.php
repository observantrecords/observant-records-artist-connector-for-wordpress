<?php


namespace ObservantRecords\WordPress\Plugins\ArtistConnector\Controllers\Api\V2;


use ObservantRecords\WordPress\Plugins\ArtistConnector\Models\Albums\Album;
use ObservantRecords\WordPress\Plugins\ArtistConnector\Models\Albums\Release;
use ObservantRecords\WordPress\Plugins\ArtistConnector\Models\Artists\Artist;

class RestApiController
{
    protected static $api_endpoint_base;

    public function __construct() {

    }

    public static function init() {
        add_action( 'rest_api_init', array( __CLASS__, 'setupCustomFields' )  );
        add_action( 'rest_api_init', array( __CLASS__, 'setupCustomEndPoints' )  );
    }

    public static function setupCustomFields() {
        register_rest_field( 'album', 'release_date', array(
                'get_callback' => array( 'ObservantRecords\WordPress\Plugins\ArtistConnector\Controllers\Api\V2\ReleaseController', 'getReleaseDate' ),
                'schema'          => null,
            )
        );

        register_rest_field( 'album', 'catalog_number', array(
                'get_callback' => array( 'ObservantRecords\WordPress\Plugins\ArtistConnector\Controllers\Api\V2\ReleaseController', 'getCatalogNumber' ),
                'schema'          => null,
            )
        );


        register_rest_field( 'album', 'artist', array(
                'get_callback' => array( 'ObservantRecords\WordPress\Plugins\ArtistConnector\Controllers\Api\V2\ReleaseController', 'getReleaseArtist' ),
                'schema'          => null,
            )
        );
    }

    public static function setupCustomEndPoints() {

        register_rest_route( 'obrc/v2', '/artist/default', array(
            'methods' => 'GET',
            'callback' => array( 'ObservantRecords\WordPress\Plugins\ArtistConnector\Controllers\Api\V2\ArtistController', 'getArtist' ),
            'args' => array(
            ),
        ) );

        register_rest_route( 'obrc/v2', '/artists', array(
            'methods' => 'GET',
            'callback' => array( 'ObservantRecords\WordPress\Plugins\ArtistConnector\Controllers\Api\V2\ArtistController', 'getAllArtists' ),
            'args' => array(
            ),
        ) );

        register_rest_route( 'obrc/v2', '/artist/(?P<artist>[\w]+)', array(
            'methods' => 'GET',
            'callback' => array( 'ObservantRecords\WordPress\Plugins\ArtistConnector\Controllers\Api\V2\ArtistController', 'getArtist' ),
            'args' => array(
                'artist'
            ),
        ) );

        register_rest_route( 'obrc/v2', '/artist/(?P<artist>[\w]+)/albums', array(
            'methods' => 'GET',
            'callback' => array( 'ObservantRecords\WordPress\Plugins\ArtistConnector\Controllers\Api\V2\AlbumController', 'getAlbumsByArtist' ),
            'args' => array(
                'artist',
                'visible',
                'format',
                'orderBy',
                'order',
                'limit',
                'showReleases',
            ),
        ) );

        register_rest_route( 'obrc/v2', '/albums', array(
            'methods' => 'GET',
            'callback' => array( 'ObservantRecords\WordPress\Plugins\ArtistConnector\Controllers\Api\V2\AlbumController', 'getAllAlbums' ),
            'args' => array(
                'visible',
                'format',
                'orderBy',
                'order',
                'limit',
                'showReleases',
            ),
        ) );

        register_rest_route( 'obrc/v2', '/album/(?P<album>[\w]+)', array(
            'methods' => 'GET',
            'callback' => array( 'ObservantRecords\WordPress\Plugins\ArtistConnector\Controllers\Api\V2\AlbumController', 'getAlbum' ),
            'args' => array(
                'album'
            ),
        ) );

        register_rest_route( 'obrc/v2', '/release/(?P<release>[\w]+)', array(
            'methods' => 'GET',
            'callback' => array( 'ObservantRecords\WordPress\Plugins\ArtistConnector\Controllers\Api\V2\ReleaseController', 'getRelease' ),
            'args' => array(
                'release'
            ),
        ) );
    }

    public static function getApiBase() {
        return get_option('observantrecords_api_endpoint');
    }

    public static function getApiArtistEndpoint() {
        $artist_endpoint_path = get_option('observantrecords_api_endpoint_artist');

        if ( empty( $artist_endpoint_path ) ) {
            return null;
        }

        $api_base = self::getApiBase();
        $connector = ( strpos( $artist_endpoint_path, '/' ) === 0) ? null : '/';
        $artist_endpoint = $api_base . $connector . get_option('observantrecords_api_endpoint_artist');

        return $artist_endpoint;
    }

    public static function query($api_url, $transient_name, $parameters = array()) {

        $cached_results = get_transient( $transient_name );

        if ( $cached_results === false ) {

            $query = urldecode( http_build_query( $parameters ) );

            $request_query = $api_url;
            if ( ! empty( $query ) ) {
                $request_query .= '?' . $query;
            }

            $response = wp_remote_get(
                $request_query,
            );

            if ( is_array( $response ) ) {
                $results = json_decode( wp_remote_retrieve_body( $response ), true );
                set_transient( $transient_name, $results, DAY_IN_SECONDS );
            } else {
                $results = $response->errors;
            }
        } else {
            $results = $cached_results;
        }

        return $results;
    }
}