<?php

namespace ObservantRecords\WordPress\Plugins\ArtistConnector\Controllers\Api\V2;

use ObservantRecords\WordPress\Plugins\ArtistConnector\Models\Albums\Album;

class AlbumController
{
    public static function getAllAlbums( $object = '' ) {
        $endpoint = RestApiController::getApiBase() . '/albums';

        $parameters = array();
        $allowed_parameters = array(
            'visible',
            'format',
            'orderBy',
            'order',
            'limit',
            'showReleases',
        );

        $available_parameters = array_filter( $allowed_parameters, function ( $parameter ) use ( $object ) {
            return ( isset( $object[$parameter] ) && !empty( $object[$parameter]) );
        });

        foreach( $available_parameters as $parameter ) {
            $parameters[$parameter] = $object[$parameter];
        }

        $query = urldecode( http_build_query( $parameters ) );

        if ( !empty( $query ) ) {
            $endpoint .= '?' . $query;
        }

        $transient_name = 'obrc_all_albums_' . md5($endpoint);

        $result = RestApiController::query($endpoint, $transient_name);

        if ( !empty( $result['data'] ) ) {
            return $result['data'];
        }

        return $result;
    }

    public static function getAlbumsByArtist( $object = '' ) {
        $endpoint = ( !empty( $object['artist']) )
            ? sprintf( '%s/artist/%s/albums', RestApiController::getApiBase(), $object['artist'])
            : sprintf( '%s/albums', RestApiController::getApiArtistEndpoint()) ;

        return self::getAlbumsByArtistPath( $endpoint, $object );
    }

    public static function getAlbumsByArtistPath( $endpoint, $object = '' ) {
        $albums = array();

        $parameters = array();
        $allowed_parameters = array(
            'visible',
            'format',
            'orderBy',
            'order',
            'limit',
            'showReleases',
        );

        $available_parameters = array_filter( $allowed_parameters, function ( $parameter ) use ( $object ) {
            return ( isset( $object[$parameter] ) && !empty( $object[$parameter]) );
        });

        foreach( $available_parameters as $parameter ) {
            $parameters[$parameter] = $object[$parameter];
        }

        $query = urldecode( http_build_query( $parameters ) );

        if ( !empty( $query ) ) {
            $endpoint .= '?' . $query;
        }

        $transient_name = 'obrc_albums_' . md5($endpoint);

        $result = RestApiController::query($endpoint, $transient_name);

        if ( isset( $result['data']) && !empty( $result['data'] ) ) {
            $albums = $result['data'];
        }

        return $albums;
    }

    public static function getAlbum( $object ) {
        $endpoint = RestApiController::getApiBase() . '/album/' . $object['album'];

        $transient_name = 'obrc_album_' . md5($endpoint);

        $result = RestApiController::query($endpoint, $transient_name);

        if ( !empty( $result['data'] ) ) {
            return $result['data'];
        }

        return $result;
    }

    public static function getAlbumByPath( $path ) {
        $endpoint = RestApiController::getApiBase() . $path;

        $transient_name = 'obrc_album_' . md5($endpoint);

        $result = RestApiController::query($endpoint, $transient_name);

        if ( !empty( $result['data'] ) ) {
            return $result['data'];
        }

        return $result;
    }
}