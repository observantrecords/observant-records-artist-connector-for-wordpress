<?php
/**
 * Created by PhpStorm.
 * User: gregbueno
 * Date: 11/8/14
 * Time: 6:39 AM
 */

namespace ObservantRecords\WordPress\Plugins\ArtistConnector\Controllers;


use Google\Protobuf\Api;
use ObservantRecords\WordPress\Plugins\ArtistConnector\Controllers\Api\V2\AlbumController;
use ObservantRecords\WordPress\Plugins\ArtistConnector\Views\BaseView;
use ObservantRecords\WordPress\Plugins\ArtistConnector\Controllers\Api\V2\RestApiController as ApiV2;

/**
 * Class PostMetaData
 * @package ObservantRecords\WordPress\Plugins\ArtistConnector
 * @author Greg Bueno
 * @copyright Observant Records
 */
class CustomFieldsController {

	/**
	 * PostMetaData constructor.
	 */
	public function __construct() {

	}

	/**
	 * init
	 *
	 * init() registers WordPress actions and filters to handle metadata for posts.
	 */
	public static function init() {
		add_action( 'add_meta_boxes', array( __CLASS__, 'addMetaBoxes' ) );
		add_action( 'save_post', array( __CLASS__, 'savePostMeta' ) );
	}

	/**
	 * addMetaBoxes
	 *
	 * addMetaBoxes() creates custom fields for all post types.
	 */
	public static function addMetaBoxes() {
        // Observant Records Metadata
        add_meta_box( 'meta_ob_album_api_path', 'Observant Records Metadata', array( __CLASS__, 'renderAlbumMetaBox' ), 'album', 'normal', 'high' );
        add_meta_box( 'meta_ob_track_id', 'Observant Records Metadata', array( __CLASS__, 'renderTrackMetaBox' ), 'track', 'normal', 'high' );

        // Relationships
        add_meta_box( 'meta_ob_artist_api_path', 'Observant Records Metadata', array( __CLASS__, 'renderArtistMetaBox' ), 'artist', 'normal', 'high' );
        add_meta_box( 'meta_ob_artist_parent', 'Relationships', array( __CLASS__, 'renderArtistParentBox' ), 'album', 'normal', 'high' );
        add_meta_box( 'meta_ob_release_parent', 'Relationships', array( __CLASS__, 'renderReleaseParentBox' ), 'track', 'normal', 'high' );

        // Shortcodes
        add_meta_box( 'meta_ob_bandcamp_shortcode', 'Shortcodes', array( __CLASS__, 'renderBandcampShortcodeBox' ), 'album', 'normal', 'high' );
        add_meta_box( 'meta_ob_bandcamp_shortcode_track', 'Shortcodes', array( __CLASS__, 'renderBandcampShortcodeBox' ), 'track', 'normal', 'high' );

        // Credits
		add_meta_box( 'meta_ob_release_credits', 'Credits', array( __CLASS__, 'renderCreditsBox' ), 'album', 'normal', 'high' );

        // Lyrics
		add_meta_box( 'meta_ob_track_lyrics', 'Lyrics', array( __CLASS__, 'renderTrackLyricsBox' ), 'track', 'normal', 'high' );
	}

	/**
	 * renderAlbumMetaBox
	 *
	 * renderAlbumMetaBox() displays input fields for album metadata.
	 *
	 * @param $post
	 */
	public static function renderAlbumMetaBox( $post ) {
        $parent_artist = get_post_meta( $post->ID, '_ob_artist_parent', true );
        $artist_api_path = null;

        if ( !empty( $parent_artist ) ) {
            $artist_endpoint = get_post_meta( $parent_artist, '_ob_artist_api_path', true );

            if ( !empty( $artist_endpoint ) ) {
                $connector = strpos( $artist_endpoint, '/' ) === 0 ? null : '/';
                $artist_api_path = ApiV2::getApiBase() . $connector . $artist_endpoint;
            }
        }

        if ( empty( $artist_api_path ) ) {
            $artist_api_path = ApiV2::getApiArtistEndpoint();
        }

        $albums = array();
        if ( !empty( $artist_api_path ) ) {
            $albums_api_path = $artist_api_path . '/albums';
            $transient_name = 'obrc_albums_' . md5($albums_api_path);

            $result = ApiV2::query( $albums_api_path, $transient_name);

            if (isset($result['data']) && !empty($result['data'])) {
                $albums = (isset($result['data']) && !empty($result['data'])) ? $result['data'] : array();
            }
        }

        $data = [
			'ob_album_alias' => get_post_meta( $post->ID, '_ob_album_alias', true ),
			'ob_release_alias' => get_post_meta( $post->ID, '_ob_release_alias', true ),
            'ob_album_api_path' => get_post_meta( $post->ID, '_ob_album_api_path', true ),
            'albums' => $albums,
		];

		BaseView::render( 'metadata/album.php', $data );
	}

    public static function renderArtistMetaBox( $post ) {
        $artist_api_path = ApiV2::getApiBase() . '/artists';

        $transient_name = 'obrc_artists_' . md5( $artist_api_path );

        $result = ApiV2::query($artist_api_path, $transient_name);

        if (isset($result['data']) && !empty($result['data'])) {
            $artists = (isset($result['data']) && !empty($result['data'])) ? $result['data'] : array();
        }

        $data = [
            'ob_artist_api_path' => get_post_meta( $post->ID, '_ob_artist_api_path', true ),
            'artists' => $artists,
        ];

        BaseView::render( 'metadata/artist.php', $data );
    }

    /**
     * renderArtistParentBox
     *
     * renderReleaseParentBox() displays input fields for the artist parent post.
     *
     * @param $post
     */
    public static function renderArtistParentBox( $post ) {

        $artists = new \WP_Query( array(
            'post_type' => 'artist',
            'posts_per_page' => -1,
            'order' => 'asc',
            'orderby' => 'post_title',
        ) );

        $data = [
            'ob_artist_parent' => get_post_meta( $post->ID, '_ob_artist_parent', true ),
            'artists' => $artists,
        ];

        BaseView::render( 'metadata/artist_parent.php' , $data );
    }


    /**
     * renderBandcampShortcodeBox
     *
     * renderBandcampShortcodeBox() displays input fields for a Bandcamp shortcode.
     *
     * @param $post
     */
    public static function renderBandcampShortcodeBox( $post ) {

        $data = [
            'ob_bandcamp_shortcode' => get_post_meta( $post->ID, '_ob_bandcamp_shortcode', true ),
        ];

        BaseView::render( 'metadata/bandcamp.php', $data );
    }

	/**
	 * renderTrackMetaBox
	 *
	 * renderTrackMetaBox() displays input fields for track metadata.
	 *
	 * @param $post
	 */
	public static function renderTrackMetaBox( $post ) {
        $artist_api_path = ApiV2::getApiArtistEndpoint();

        $albums = array();
        if (!empty($artist_api_path)) {
            $transient_name = 'obrc_artist' . md5($artist_api_path);

            $result = ApiV2::query($artist_api_path, $transient_name);

            if (isset($result['data']) && !empty($result['data'])) {
                $albums = (isset($result['data']['albums']) && !empty($result['data']['albums'])) ? $result['data']['albums'] : array();
            }
        }
        $album_api_path = get_post_meta( $post->ID, '_ob_album_api_path', true );

        $tracks = array();
        if ( !empty( $album_api_path ) && !empty( $albums ) ) {
            $album_filter = array_filter( $albums, function ( $album ) use ( $album_api_path ) {
                return $album['api_path'] == $album_api_path;
            } );
            $album = ( !empty( $album_filter ) ) ? array_pop( $album_filter ) : array();

            if ( !empty( $album ) ) {
                $tracks = $album['primary_release']['tracks'];
            }
        }

		$data = [
			'ob_release_alias' => get_post_meta( $post->ID, '_ob_release_alias', true ),
			'ob_track_alias' => get_post_meta( $post->ID, '_ob_track_alias', true ),
            'ob_album_api_path' => get_post_meta( $post->ID, '_ob_album_api_path', true ),
            'ob_track_api_path' => get_post_meta( $post->ID, '_ob_track_api_path', true ),
            'albums' => $albums,
            'tracks' => $tracks,
		];

		BaseView::render( 'metadata/track.php' , $data );
	}

    /**
     * renderReleaseParentBox
     *
     * renderReleaseParentBox() displays input fields for the release parent post.
     *
     * @param $post
     */
    public static function renderReleaseParentBox( $post ) {

        $albums = new \WP_Query( array(
            'post_type' => 'album',
            'posts_per_page' => -1,
            'order' => 'asc',
            'orderby' => 'post_title',
        ) );

        $data = [
            'ob_release_parent' => get_post_meta( $post->ID, '_ob_release_parent', true ),
            'albums' => $albums,
        ];

        BaseView::render( 'metadata/release_parent.php' , $data );
    }

	/**
	 * renderTrackLyricsBox
	 *
	 * renderTrackLyricsBox() displays input fields for track lyrics.
	 *
	 * @param $post
	 */
	public static function renderTrackLyricsBox( $post ) {
		$ob_track_lyrics = get_post_meta( $post->ID, '_ob_track_lyrics', true );

		wp_editor( $ob_track_lyrics, 'ob_track_lyrics', array(
			'media_buttons' => false,
		) );
		wp_nonce_field( 'lyrics_nonce', 'lyrics_name' );
	}

	/**
	 * renderCreditsBox
	 *
	 * renderCreditsBox() displays input fields for album credits.
	 *
	 * @param $post
	 */
	public static function renderCreditsBox( $post ) {
		$ob_release_credits = get_post_meta( $post->ID, '_ob_release_credits', true );

		wp_editor( $ob_release_credits, 'ob_release_credits', array(
			'media_buttons' => false,
		) );
		wp_nonce_field( 'credits_nonce', 'credits_name' );
	}

	/**
	 * savePostMeta
	 *
	 * savePostMeta() stores or destroys values for custom fields.
	 *
	 * @param $post_id
	 */
	public static function savePostMeta( $post_id ) {
		if ( isset( $_POST['ob_album_alias'] ) ) {
			$ob_album_alias = $_POST['ob_album_alias'];

			if ( !empty( $_POST ) && check_admin_referer( 'meta_nonce', 'meta_name' ) ) {
				empty( $ob_album_alias ) ? delete_post_meta( $post_id, '_ob_album_alias' ) : update_post_meta( $post_id, '_ob_album_alias', $ob_album_alias );
			}
		}
        if ( isset( $_POST['ob_album_api_path'] ) ) {
            $ob_album_api_path = $_POST['ob_album_api_path'];

            if ( !empty( $_POST ) && check_admin_referer( 'meta_nonce', 'meta_name' ) ) {
                empty( $ob_album_api_path ) ? delete_post_meta( $post_id, '_ob_album_api_path' ) : update_post_meta( $post_id, '_ob_album_api_path', $ob_album_api_path );
            }
        }
        if ( isset( $_POST['ob_artist_parent'] ) ) {
            $ob_artist_parent = $_POST['ob_artist_parent'];

            if ( !empty( $_POST ) && check_admin_referer( 'artist_nonce', 'artist_name' ) ) {
                empty( $ob_artist_parent ) ? delete_post_meta( $post_id, '_ob_artist_parent' ) : update_post_meta( $post_id, '_ob_artist_parent', $ob_artist_parent );
            }
        }
        if ( isset( $_POST['ob_artist_api_path'] ) ) {
            $ob_artist_api_path = $_POST['ob_artist_api_path'];

            if ( !empty( $_POST ) && check_admin_referer( 'meta_nonce', 'meta_name' ) ) {
                empty( $ob_artist_api_path ) ? delete_post_meta( $post_id, '_ob_artist_api_path' ) : update_post_meta( $post_id, '_ob_artist_api_path', $ob_artist_api_path );
            }
        }
		if ( isset( $_POST['ob_release_alias'] ) ) {
			$ob_release_alias = $_POST['ob_release_alias'];

			if ( !empty( $_POST ) && check_admin_referer( 'meta_nonce', 'meta_name' ) ) {
				empty( $ob_release_alias ) ? delete_post_meta( $post_id, '_ob_release_alias' ) : update_post_meta( $post_id, '_ob_release_alias', $ob_release_alias );
			}
		}
		if ( isset( $_POST['ob_track_alias'] ) ) {
			$ob_track_alias = $_POST['ob_track_alias'];

			if ( !empty( $_POST ) && check_admin_referer( 'meta_nonce', 'meta_name' ) ) {
				empty( $ob_track_alias ) ? delete_post_meta( $post_id, '_ob_track_alias' ) : update_post_meta( $post_id, '_ob_track_alias', $ob_track_alias );
			}
		}
        if ( isset( $_POST['ob_track_api_path'] ) ) {
            $ob_track_api_path = $_POST['ob_track_api_path'];

            if ( !empty( $_POST ) && check_admin_referer( 'meta_nonce', 'meta_name' ) ) {
                empty( $ob_track_api_path ) ? delete_post_meta( $post_id, '_ob_track_api_path' ) : update_post_meta( $post_id, '_ob_track_api_path', $ob_track_api_path );
            }
        }
		if ( isset( $_POST['ob_release_credits'] ) ) {
			$ob_release_credits = $_POST['ob_release_credits'];

			if ( !empty( $_POST ) && check_admin_referer( 'credits_nonce', 'credits_name' ) ) {
				empty( $ob_release_credits ) ? delete_post_meta( $post_id, '_ob_release_credits' ) : update_post_meta( $post_id, '_ob_release_credits', $ob_release_credits );
			}
		}
		if ( isset( $_POST['ob_track_lyrics'] ) ) {
			$ob_track_lyrics = $_POST['ob_track_lyrics'];

			if ( !empty( $_POST ) && check_admin_referer( 'lyrics_nonce', 'lyrics_name' ) ) {
				empty( $ob_track_lyrics ) ? delete_post_meta( $post_id, '_ob_track_lyrics' ) : update_post_meta( $post_id, '_ob_track_lyrics', $ob_track_lyrics );
			}
		}
        if ( isset( $_POST['ob_bandcamp_shortcode'] ) ) {
            $ob_bandcamp = $_POST['ob_bandcamp_shortcode'];

            if ( !empty( $_POST ) && check_admin_referer( 'bandcamp_nonce', 'bandcamp_name' ) ) {
                empty( $ob_bandcamp ) ? delete_post_meta( $post_id, '_ob_bandcamp_shortcode' ) : update_post_meta( $post_id, '_ob_bandcamp_shortcode', $ob_bandcamp );
            }
        }
        if ( isset( $_POST['ob_release_parent'] ) ) {
            $ob_release_parent = $_POST['ob_release_parent'];

            if ( !empty( $_POST ) && check_admin_referer( 'album_nonce', 'album_name' ) ) {
                empty( $ob_release_parent ) ? delete_post_meta( $post_id, '_ob_release_parent' ) : update_post_meta( $post_id, '_ob_release_parent', $ob_release_parent );
            }
        }
	}

} 