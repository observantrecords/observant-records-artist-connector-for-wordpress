<?php
/**
 * Created by PhpStorm.
 * User: gregbueno
 * Date: 11/8/14
 * Time: 6:19 AM
 */

namespace ObservantRecords\WordPress\Plugins\ArtistConnector;

use ObservantRecords\WordPress\Plugins\ArtistConnector\Controllers\Api\V1\RestApiController as ApiV1;
use ObservantRecords\WordPress\Plugins\ArtistConnector\Controllers\Api\V2\RestApiController as ApiV2;
use ObservantRecords\WordPress\Plugins\ArtistConnector\Controllers\Api\V1\BlockController as BlockControllerV1;
use ObservantRecords\WordPress\Plugins\ArtistConnector\Controllers\Api\V2\BlockController as BlockControllerV2;
use ObservantRecords\WordPress\Plugins\ArtistConnector\Controllers\CustomFieldsController;
use ObservantRecords\WordPress\Plugins\ArtistConnector\Controllers\CustomPostTypesController;
use ObservantRecords\WordPress\Plugins\ArtistConnector\Controllers\SettingsController;

/**
 * Use this constant for functions with an argument for scope.
 */
const WP_PLUGIN_DOMAIN = 'observantrecords_artist_connector';

/**
 * Class Setup
 * @package ObservantRecords\WordPress\Plugins\ArtistConnector
 * @author Greg Bueno
 * @copyright Observant Records
 */
class Setup {

	/**
	 * Setup constructor.
	 */
	public function __construct() {

	}

	/**
	 * init
	 *
	 * init() registers WordPress actions and filters to setup the plugin.
	 */
	public static function init() {
	    SettingsController::init();
        CustomPostTypesController::init();
	    CustomFieldsController::init();
        ApiV1::init();
        ApiV2::init();
	    BlockControllerV2::init();
	}

	/**
	 * activate
	 *
	 * Use this method with register_activation_hook().
	 */
	public static function activate() {
		delete_option('aws_secret_key');
	}

	/**
	 * deactivate
	 *
	 * Use this method with register_deactivation_hook.
	 */
	public static function deactivate() {

	}

	/**
	 * uninstall
	 *
	 * Use this method with register_uninstall_hook().
	 */
	public static function uninstall() {

	}

}