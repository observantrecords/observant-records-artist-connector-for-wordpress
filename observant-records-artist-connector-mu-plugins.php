<?php
/**
 * Plugin Name: Observant Records Artist Connector
 * Plugin URI: https://bitbucket.org/observantrecords/observant-records-artist-connector-for-wordpress
 * Description: This custom plugin connects to the Observant Records Artist database.
 * Version: 3.2.6
 * Author: Greg Bueno
 * Author URI: http://vigilantmedia.com
 * License: MIT
 */

/*
 * This file is available for must-use plugin installation.
 *
 * 1. Install this plugin in the wp-content/mu-plugins directory.
 * 2. Copy or move this file to the root level of the mu-plugins directory.
 * 3. Rename this file to observant-records-artist-connector.php.
 */

require WPMU_PLUGIN_DIR.'/observant-records-artist-connector/observant-records-artist-connector.php';