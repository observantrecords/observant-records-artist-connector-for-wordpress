import apiFetch from '@wordpress/api-fetch';
import { createReduxStore, register } from '@wordpress/data';

const DEFAULT_STATE = {
    release: {},
};

const STORE_NAME = 'obrc/release';

const actions = {
    setRelease: ( item, release ) => {
        return {
            type: 'SET_RELEASE',
            item,
            release,
        };
    },
};

const releaseStore = createReduxStore( STORE_NAME, {
    reducer: ( state = DEFAULT_STATE, action ) => {
        switch( action.type ) {
            case 'SET_RELEASE':
                return {
                    ...state,
                    release: {
                        ...state.release,
                        [ action.item ]: action.release,
                    }
                };
        }

        return state;
    },
    actions,
    selectors: {
        getRelease: ( state, item ) => {
            const { release } = state;
            const album = release[ item ];

            return release;
        },
    },
    resolvers: {
        getRelease: ( item ) => async( {dispatch} ) => {
            const path = '/obrc/v1/release/' + item;
            const release = apiFetch( { path } );
            dispatch.setRelease( item, release );
        }
    },
} );
register( releaseStore );

export { STORE_NAME };