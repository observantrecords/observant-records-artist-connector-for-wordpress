import apiFetch from '@wordpress/api-fetch';
import { createReduxStore, register } from '@wordpress/data';

const DEFAULT_STATE = {
    artists: [],
    artistDefault: {},
};

const ARTIST_STORE = 'obrc/artists';

const actions = {
    setArtists: ( artists ) => {
        return {
            type: 'SET_ARTISTS',
            artists,
        };
    },
    setArtist: ( item, artist ) => {
        return {
            type: 'SET_ARTIST',
            item,
            artist,
        };
    },
    setArtistDefault: ( artist_default ) => {
        return {
            type: 'SET_ARTIST_DEFAULT',
            artist_default,
        }
    }
};

const artistStore = createReduxStore( ARTIST_STORE, {
    reducer: ( state = DEFAULT_STATE, action ) => {
        switch( action.type ) {
            case 'SET_ARTISTS':
                return {
                    ...state,
                    artists: action.artists
                };
            case 'SET_ARTIST':
                return {
                    ...state,
                    artists: {
                        ...state,
                        [ action.item ]: action.artist,
                    }
                };
            case 'SET_ARTIST_DEFAULT':
                return {
                    ...state,
                    artistDefault: action.artist_default,
                }
        }

        return state;
    },
    actions,
    selectors: {
        getArtists: ( state ) =>  {
            const { artists } = state;
            return artists;
        },
        getArtist: ( state, item ) => {
            const { artists } = state;
            return artists[ item ];
        },
        getArtistDefault: ( state ) => {
            const { artistDefault } = state;
            console.log( state );
            return artistDefault;
        }
    },
    resolvers: {
        getArtists: () => async ( {dispatch} ) => {
            const path = '/obrc/v2/artists';
            const artists = await apiFetch( { path } );
            dispatch.setArtists( artists );
        },
        getArtist: ( item ) => async ( {dispatch} ) => {
            const path = '/obrc/v2/artist/' + item;
            const artist = await apiFetch( { path } );
            dispatch.setArtist( item, artist );
        },
        getArtistDefault: () => async ( {dispatch} ) => {
            const path = '/obrc/v2/artist/default';
            const artist_default = await apiFetch( { path } );
            dispatch.setArtistDefault( artist_default );
        },
    },
} );

register( artistStore );

export { ARTIST_STORE };