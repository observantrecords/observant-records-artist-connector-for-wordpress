import { ARTIST_STORE } from "./artist";
import { ALBUM_STORE } from "./album";
import { registerBlockType } from '@wordpress/blocks';
import { withSelect } from '@wordpress/data';
import { InspectorControls } from '@wordpress/block-editor';
import { PanelBody, PanelRow, __experimentalNumberControl as NumberControl, SelectControl } from '@wordpress/components';
import ServerSideRender from '@wordpress/server-side-render';
import { withState } from '@wordpress/compose';

registerBlockType( 'observant-records/release-list', {
    title: 'Release List',
    icon: 'album',
    category: 'widgets',
    attributes: {
        postsToShow: {
            type: 'number',
            default: 4
        },
        artist: {
            type: 'string',
        },
        artistDefault: {
            type: 'object',
        }
    },
    save: () => {
        return null;
    },
    edit: withSelect( ( select, props ) => {
        const { artist } = props.attributes;
        let albums = null;
        let artistList = null;
        const artists = select( ARTIST_STORE).getArtists();
        const artistDefault = select( ARTIST_STORE ).getArtistDefault();

        if ( artistDefault.id !== undefined ) {
            artistList = artists.filter( ( artist ) => {
                return artist.id === artistDefault.id;
            }, artistDefault);
        } else {
            artistList = artists;
        }

        if ( artist === 'all' || typeof artist === 'undefined' ) {
            albums = select( ALBUM_STORE ).getAlbums();
        } else {
            albums = select( ALBUM_STORE ).getArtistAlbums( artist );
        }

        return {
            artists: artistList,
            albums: albums,
        };
    } )( ( props ) => {
        const { attributes: { postsToShow, artist }, setAttributes, albums, artists, className } = props;

        const onChangePostToShow = ( value ) => {
            setAttributes( { postsToShow: parseInt( value )  } );
        };

        const onChangeArtists = ( value ) => {
            setAttributes( { artist: value } );
        }

        if ( !artists ) {
            return 'Loading ...';
        }

        if ( artists && artists.length === 0 ) {
            return 'No artists yet.';
        }

        const artistList = artists.map( ( artist ) => {
            return {
                label: artist.display_name,
                value: artist.id,
            };
        } );

        if ( !albums ) {
            return 'Loading ...';
        }

        if ( albums && albums.length === 0 ) {
            return 'No albums yet.';
        }

        const albumListAll = albums.map( ( album ) => <li>
            { album.artist }: { album.title }
        </li> );
        let albumList = ( postsToShow > 0 ) ? albumListAll.slice(0 , postsToShow) : albumListAll;

        return <div className={ className }>
            <InspectorControls>
                <PanelBody title={ "Settings" }>
                    <PanelRow>
                        <SelectControl
                            label={ "Artist" }
                            value={ artist }
                            onChange={ onChangeArtists }
                            options={
                                [
                                    { label: 'All', value: 'all' },
                                    ...artistList
                                ]
                            }
                        />
                    </PanelRow>
                    <PanelRow>
                        <NumberControl
                            label={ "Number of posts"}
                            onChange={ onChangePostToShow }
                            value={ postsToShow }
                            min={ 0 }
                        />
                    </PanelRow>
                </PanelBody>
            </InspectorControls>
            <ServerSideRender
              block="observant-records/release-list"
              attributes={ {
                  artist: artist,
                  postsToShow: postsToShow,
              } }
            />
        </div>
    } ),
} );
