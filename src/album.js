import addQueryArgs from '@wordpress/url';

import apiFetch from '@wordpress/api-fetch';
import { createReduxStore, register } from '@wordpress/data';

const DEFAULT_STATE = {
    albums: [],
};

const ALBUM_STORE = 'obrc/albums';

const actions = {
    setAlbums: ( albums ) => {
        return {
            type: 'SET_ALBUMS',
            albums,
        };
    },
    setAlbum: ( item, album ) => {
        return {
            type: 'SET_ALBUM',
            item,
            album,
        };
    },
};

const albumStore = createReduxStore( ALBUM_STORE, {
    reducer: ( state = DEFAULT_STATE, action ) => {
        switch( action.type ) {
            case 'SET_ALBUMS':
                return {
                    ...state,
                    albums: action.albums
                };
            case 'SET_ALBUM':
                return {
                    ...state,
                    albums: {
                        ...state,
                        [ action.item ]: action.album,
                    }
                };
        }

        return state;
    },
    actions,
    selectors: {
        getAlbums: ( state ) => {
            const { albums } = state;
            return albums;
        },
        getArtistAlbums: ( state, item ) => {
            const { albums } = state;
            return albums;
        },
        getAlbum: ( state, item ) => {
            const { albums } = state;
            return albums[ item ];
        },
    },
    resolvers: {
        getArtists: () => async ( {dispatch} ) => {
            const path = '/obrc/v2/artists';
            const artists = await apiFetch( { path } );
            dispatch.setArtists( artists );
        },
        getAlbums: () => async( {dispatch} ) => {
            const path = '/obrc/v2/albums?visible=1&orderBy=album_release_date&order=asc';
            const albums = await apiFetch( {path} );
            dispatch.setAlbums( albums );
        },
        getArtistAlbums: ( item ) => async( {dispatch } ) => {
            const path = '/obrc/v2/artist/' + item + '/albums?visible=1&orderBy=album_release_date&order=desc';
            const albums = await apiFetch( { path } );
            dispatch.setAlbums( albums );
        },
        getAlbum: ( item ) => async ( {dispatch} ) => {
            const path = '/obrc/v2/album/' + item;
            const album = await apiFetch( { path } );
            dispatch.setAlbum( item, album );
        },
    },
} );
register( albumStore );

export { ALBUM_STORE };